import requests
import pandas as pd
from bs4 import BeautifulSoup

def main_function():

    job_data_dict = None

    keyword_input = input("Input a keyword to search(0=Exit): ")
    if keyword_input == "0":
        print("Goodbye!")
        exit()
        
    count_input = input("How many jobs do you want?(0=Exit) ")
    if count_input == "0":
        main_function()

    job_data_dict = get_Data(keyword_input, int(count_input))

    df = pd.DataFrame(job_data_dict)
    df.index.name = "Index"
    df.index += 1
    num_of_rows = len(df.index)
    print(df)

    df.to_csv(r'C:\Users\Hin\Desktop\repo\DatatonProject\jobsdb.csv',mode='a', header=True, encoding='utf_8_sig') 
    print("{} jobs find".format(num_of_rows))
    print("Save to csv file successfully!")
    main_function()

def request_URL(keyword, pages=1):

    url = "https://hk.jobsdb.com/hk/search-jobs/{}/{}".format(keyword, pages)

    req = requests.get(url)   
    soup = BeautifulSoup(req.content, 'html.parser')
    result = soup.prettify()

    return soup

def get_Num_Of_Pages(soup):

    page_list = []
    try:
        pages = soup.find("select", class_="FYwKg _3j_fQ _2PHih _3sxhA af1kF_0 _20Cd9_0 _3VCZm _3RqUb_0 _QLQx Of_Sx_0 _2qcxd _3_RCw_0 C6ZIU_0 _6ufcS_0 _2DNlq_0 _29m7__0 _1W_Sg_0")
        for page in pages:
            page_list.append(page.text) 
    except:
        page_list.append(1)
         
    return int(page_list[-1])

def get_Data(keyword, count_input):

    job_data_dict = {"Title":[],
                     "KeyWord":[],
                     "Source":[],
                     "Company":[],
                     "Location":[],  
                     "Salary":[],
                     "Job Highlights":[],
                     "URL":[]             
                    }

    pages = get_Num_Of_Pages(request_URL(keyword))
    count = 0

    for i in range(1,pages+1):

        soup = request_URL(keyword, i)

        #get how many gird of job's
        info_s = soup.find_all("div", class_="FYwKg _1GAuD fB92N_0 _1pAdR_0 FLByR_0 _2QIfI_0 _3ftyQ _1lyEa _1N8Dy")
       
        #loop to find the information   
        for info in info_s:
            if count < count_input:

                job_data_dict["Title"].append(info.find("h1", class_="FYwKg _2Bz3E C6ZIU_0 _6ufcS_0 _2DNlq_0 sQuda_0").text) 
                job_data_dict["KeyWord"].append(keyword.upper())
                job_data_dict["Source"].append("JobsDB")

                #Find Company Name
                try: 
                    job_data_dict["Company"].append(info.find("span", class_ = "FYwKg _2Bz3E C6ZIU_0 _6ufcS_0 _2DNlq_0 _29m7__0").text)   
                except:
                    job_data_dict["Company"].append(info.find("span", class_ = "FYwKg _2mOt7_0 PrHFr").text)
                    
                #Find Location
                try: 
                    job_data_dict["Location"].append(info.find_all("span", class_="FYwKg _2Bz3E C6ZIU_0 _1_nER_0 _2DNlq_0 sQuda_0")[0].text)   
                except:
                    job_data_dict["Location"].append("Others")
                
                #Find Salary
                try: 
                    job_data_dict["Salary"].append(info.find_all("span", class_="FYwKg _2Bz3E C6ZIU_0 _1_nER_0 _2DNlq_0 sQuda_0")[1].text)   
                except:
                    job_data_dict["Salary"].append("No Provided")
                    
                #Find Job Highlights
                num = 0
                highlight = ""
                highlights = info.find("ul", class_="FYwKg _302h6 d7v3r _2uGS9_0").find_all("li", class_="FYwKg zoxBO_0")

                if not highlights:
                    highlight = "No Provided" 
                else:      
                    for i in highlights:
                        num += 1
                        highlight += "{}. {} ".format(num,i.text)   
                            
                job_data_dict["Job Highlights"].append(highlight)
                    
                #Find URL
                try: 
                    job_data_dict["URL"].append("https://hk.jobsdb.com{}".format(info.find("a")['href']))   
                except:
                    job_data_dict["URL"].append("No Provided")
                    
                count += 1
            else:
                return job_data_dict
    return job_data_dict
#Run-------------------------------------------------------------------------------------------------------------
main_function()

